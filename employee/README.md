### Getting Started

This app has three components.

1. A REST API which receives a message from the consumer.
2. A Kafka producer which post the message received by the API into the Kafka topic. 
3. A Kafka consumer which pulls the message from Kafka topic and inserts into Postgres DB.

Steps to setup the project:

Run `git clone https://gitlab.com/Shukla001/eventdriven-microservice.git` <br/>
Run `cd:\eventdriven-microservice\employee` <br/>
Run `docker build -t employee:0.0.1-SNAPSHOT .`<br/>
Run `docker-compose up -d`<br/>
`POST` request with below payload using postman to the url [http://localhost:8080/employee/](http://localhost:8080/employee/)<br/>
Payload <br/>
```javascript
{
	"id":1,
	"name":"Jitendra Shukla",
	"dept": "IT"
}
```
`GET` request to the URL [http://localhost:8080/employee/](http://localhost:8080/employee/)

Result:

```javascript
[
 {
	"id":1,
	"name":"Jitendra Shukla",
	"dept": "IT"
 }
]
```



