package com.microservices.employee.bindings;

import com.microservices.employee.model.Employee;
import com.microservices.employee.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding(Sink.class)
public class Consumer {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private final EmployeeRepository repository;

    public Consumer(EmployeeRepository repository) {
        this.repository = repository;
    }

    @StreamListener(target = Sink.INPUT)
    public void consume(Employee emp) {
        logger.info("Received a message : " + emp);
        repository.save(emp);
    }
}
