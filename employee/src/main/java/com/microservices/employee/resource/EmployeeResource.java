package com.microservices.employee.resource;

import com.microservices.employee.bindings.Producer;
import com.microservices.employee.model.Employee;
import com.microservices.employee.repository.EmployeeRepository;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeResource {

    private Producer producer;
    private EmployeeRepository repository;

    public EmployeeResource(Producer producer, EmployeeRepository repository) {
        this.producer = producer;
        this.repository = repository;
    }

    @PostMapping
    public String putMsgInKafkaTopic(@RequestBody Employee emp){
        producer.getSource()
                .output()
                .send(MessageBuilder.withPayload(emp)
                        .setHeader("type", "application/json")
                        .build());
        return "success";
    }

    @GetMapping
    public List<Employee> findAll(){
        return repository.findAll();
    }
}
